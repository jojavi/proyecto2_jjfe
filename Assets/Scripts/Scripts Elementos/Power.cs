using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Power : MonoBehaviour
{

    public float growpup = 2f;
    public int timepower  = 5;
     private void OnTriggerEnter2D(Collider2D other) {
       
        Debug.Log(" ingresa aqui ");

        if (other.CompareTag("Player")) { 
            
            StartCoroutine (getPower(other));            
         }
       
        
    }
    public IEnumerator  getPower (Collider2D player)
    {
        
         player.transform.localScale *= growpup;


        yield return new WaitForSeconds(timepower);
        
         Destroy (gameObject);
    }
   
}
