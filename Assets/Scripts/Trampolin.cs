using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampolin : MonoBehaviour
{

    private GroundMover groundMover;
    // Start is called before the first frame update
     

    private void Start() {
         groundMover = GetComponent<GroundMover>();
    }
    private void OnCollisionStay2D(Collision2D other) {
       groundMover.jump(true);

    }


}
