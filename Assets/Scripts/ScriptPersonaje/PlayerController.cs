using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem; 

public class PlayerController : MonoBehaviour
{

   // [SerializeField] private GroundMover groundMover;
   private GroundMover groundMover;
    private Vector2 moveInput;

    private bool corriendo = false;

    private ProjectileSpawner projectileSpawner;

    // Start is called before the first frame update
    //private GroundMover groundMover2();
    void Start()
    {
        groundMover = GetComponent<GroundMover>();
        projectileSpawner = GetComponent<ProjectileSpawner>();
    }

    // Update is called once per frame
    void Update()
    {  

    ///if (corriendo){
      //   groundMover.move(moveInput.x, true); 
       
    //  }
     // else
    //  {    
         groundMover.move(moveInput.x,false); 
       
    //  }



        if(Mathf.Abs(moveInput.x)> Mathf.Epsilon)
        {
            groundMover.FlipTransform(moveInput.x);
        }

        
        
    }

    private void FixedUpdate() {
        
     //groundMover.move(moveInput.x);
      groundMover.Climb(moveInput.y);
       
    }

    public void OnMove(InputAction.CallbackContext context)
    
    {

        
        moveInput = context.ReadValue<Vector2>();

        
    }

      public void  OnRun(InputAction.CallbackContext context)
    {
        
       /* if (context.performed) { 
            //groundMover.move(moveInput.x, true);
            this.corriendo = true;
            Debug.Log("corriendo");
            
         }
        if (context.canceled) { 
           //groundMover.move(moveInput.x, false);
           this.corriendo = false;
           Debug.Log("caminando");
           
        }*/

        corriendo=  context.action.triggered;
        
    }

    public void OnJump(InputAction.CallbackContext context)

    {

        groundMover.jump(context.action.triggered);

    }

      public void OnFire(InputAction.CallbackContext context)

    {

        if (!context.action.triggered) { return; }
        else{
        projectileSpawner.Fire();
        }

    }


}
